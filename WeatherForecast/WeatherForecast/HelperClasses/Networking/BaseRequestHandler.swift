//
//  BaseRequestHanlder.swift
//  PopNetwork
//
//  Created by Omar on 6/9/18.
//  Copyright © 2018 Omar. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

typealias JSONTaskCompletionHandler<T> = ((NetworkResponse<T>) -> Void)?

protocol BaseRequestHandler:BaseResponseHandler {
    func send<T: Codable>(_ decoder: T.Type, completion: JSONTaskCompletionHandler<T>)
}

extension BaseRequestHandler where Self: BaseRequestBuilder {
    
    func send<T: Codable>(_ decoder: T.Type, completion: JSONTaskCompletionHandler<T>) {
        request(self).responseData { (response) in self.handleResponse(response, completion: completion) }
    }
}




