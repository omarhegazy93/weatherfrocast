//
//  NetworkResponse.swift
//  PopNetwork
//
//  Created by Omar on 6/12/18.
//  Copyright © 2018 Omar. All rights reserved.
//

import Foundation

enum NetworkResponse<T> {
    case success(T), error(String?)
}
