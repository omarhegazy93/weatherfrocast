//
//  BaseResponseHandler.swift
//  PopNetwork
//
//  Created by Omar on 6/9/18.
//  Copyright © 2018 Omar. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol BaseResponseHandler {
    func handleResponse<T: Codable>(_ response: DataResponse<Data>, completion: JSONTaskCompletionHandler<T>)
}

extension BaseResponseHandler {
    
    func handleResponse<T: Codable>(_ response: DataResponse<Data>, completion: JSONTaskCompletionHandler<T>) {
        switch response.result {
        case .failure(let error):
            completion?(NetworkResponse<T>.error(error.localizedDescription))
            AppRefrence.showErrorMessage(title: "", body: error.localizedDescription)
            
        case .success(let value):
            handleResponseData(value, completion: completion)
        }
    }
    
    private func handleResponseData<T: Codable>(_ data:Data, completion: JSONTaskCompletionHandler<T>) {
        do {
            print("Response \(JSON(data))")
            let obj = try JSONDecoder().decode(T.self, from: data)
            completion?(NetworkResponse<T>.success(obj))
            
        } catch let jsonError {
            completion?(NetworkResponse<T>.error(jsonError.localizedDescription))
            print("JsonSerlization Error  \(jsonError.localizedDescription)")
        }
    }
}

