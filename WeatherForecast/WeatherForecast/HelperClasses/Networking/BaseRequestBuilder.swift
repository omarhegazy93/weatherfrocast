//
//  BaseRequest.swift
//  PopNetwork
//
//  Created by Omar on 6/9/18.
//  Copyright © 2018 Omar. All rights reserved.
//

import Foundation
import Alamofire

public enum RequestEncoding {
    case queryString
    case requestBody
    case multiPartFormData
}

protocol BaseRequestBuilder: URLRequestConvertible,BaseRequestHandler {
    
    var mainURL: URL { get }
    var requestURL: URL { get }
    
    // MARK: - Path
    var path: String { get }
    
    // MARK: - Parameters
    var parameters: Parameters { get }
    
    // MARK: - Methods
    var method: HTTPMethod { get }
    
    // MARK: - Headers
    var headers: [String:String] { get }
    
    var encoding: RequestEncoding { get }
    
    var urlRequest: URLRequest { get }
    
}

//default initialization
extension BaseRequestBuilder {
    
    var encoding: RequestEncoding {
        switch method {
        default:
            return .queryString
        }
    }
    
    var mainURL: URL  {
        return URL(string: APIConstants.BASE_URL)!
    }
    
    var method: HTTPMethod  {
        return .get
    }
    
    var parameters: Parameters  {
        return [:]
    }
    
    var headers: [String:String]  {
        return [:]
    }
    
    
    var requestURL: URL {
        let strUrl = mainURL.appendingPathComponent(path).absoluteString
        return URL(string: strUrl)!
    }
    
    var urlRequest: URLRequest {
        var request = URLRequest(url: requestURL)
        request.httpMethod = method.rawValue
        
        request.httpBody = httpBody
        request.url = url
        
        allHTTPHeaderFields.forEach {
            request.setValue($0.value, forHTTPHeaderField: $0.key)
        }
        
        return request
    }
    
    
    private var httpBody: Data? {
        guard encoding == .requestBody, !parameters.isEmpty else { return nil }
        do {
            let jsonAsData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            return jsonAsData
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    private var url: URL {
        guard encoding == .queryString, !parameters.isEmpty,
            var urlComponents = URLComponents(
                url: requestURL,
                resolvingAgainstBaseURL: false) else {
                    return requestURL
        }
        
        urlComponents.queryItems = [URLQueryItem]()
        parameters.forEach {
            let queryItem = URLQueryItem(
                name: $0.key,
                value: "\($0.value)")
            urlComponents.queryItems?.append(queryItem)
        }
        return urlComponents.url ?? requestURL
        
    }
    
    private var allHTTPHeaderFields: [String: String] {
        var _headers: [String:String] = headers
        
        switch encoding {
        case .multiPartFormData:
            _headers["Content-Type"] = "multipart/form-data"
        case .requestBody:
            _headers["Content-Type"] = "application/json"
        case .queryString:
            _headers["Content-Type"] = "application/x-www-form-urlencoded"
        }
        return _headers
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        print("\(method.rawValue) --  \(requestURL.absoluteString)")
        print("Params --- \(parameters) --")
        print("headers --- \(headers) --")
        return urlRequest
    }
}

