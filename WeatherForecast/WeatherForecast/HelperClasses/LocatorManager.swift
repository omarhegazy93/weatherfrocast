//
//  LocatorMng.swift
//  Tarwe2a
//
//  Created by Omar on 5/30/19.
//  Copyright © 2019 Omar. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

enum LocationStatus{
    case enabled
    case disabled
    case notDetermined
}

struct UserLocation {
    var lat: Double?
    var lang: Double?
    var cityName: String?
    var country: String?
    
    init(coord: CLLocationCoordinate2D, cityName: String, country: String) {
        self.lang = coord.longitude
        self.lat = coord.latitude
        self.cityName = cityName
        self.country = country
    }
}

class LocatorManager: NSObject {
    
    typealias CompletionHandler = (_ userLocation: UserLocation?,_ error: String?)->Void
    
    enum LocationManageState {
        case failed
        case updating
        case stoped
        case paused
    }
    
    enum LocationManageMode {
        case oneShot
        case oneShotWithAddress
        case continues
    }
    
    /// Core location
    private let locationManager: CLLocationManager =  CLLocationManager()
    
    /// Location manager current state
    private var state = LocationManageState.stoped
    
    /// Location manager current state
    private var mode = LocationManageMode.oneShot
    
    /// Location
    private var currentLocation = CLLocation()
    
    static let shared: LocatorManager = {
        let instance = LocatorManager()
        return instance
    }()
    
    
    private var completion:CompletionHandler?
    
    ///Start Location updation
    override init() {
        super.init()
    }
    
    var isLocationServiceEnabled: Observable<LocationStatus> {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .notDetermined:
                    return .init(value: .notDetermined)
            case  .restricted, .denied:
                return .init(value: .disabled)
                case .authorizedAlways, .authorizedWhenInUse:
                    return .init(value: .enabled)
                @unknown default:
                    return .init(value: .disabled)
            }
        } else { return .init(value: .disabled) }
    }
    
    
    func getCurrentLocationGeocoder(completion:@escaping CompletionHandler) {
        self.completion = completion
        self.mode = .oneShotWithAddress
        self.startUpdating()
    }
    
    func getCurrentLocation(completion:@escaping CompletionHandler) {
        self.completion = completion
        self.mode = .oneShot
        self.startUpdating()
    }
    
    func subscribeLocationUpdates(completion:@escaping CompletionHandler) {
        self.completion = completion
        self.mode = .continues
        self.startUpdating()
    }
    
    private func startUpdating() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.activityType = .automotiveNavigation
        locationManager.distanceFilter = 100
        //        locationManager.allowsBackgroundLocationUpdates = true
        
        let status = CLLocationManager.authorizationStatus()
        
        if(status == .denied || status == .restricted || !CLLocationManager.locationServicesEnabled()) {
            // show alert to user telling them they need to allow location data to use some feature of your app
            displayAlertWithTitleMessageAndTwoButtons()
            return
        }
        
        // if haven't show location permission dialog before, show it to user
        if(status == .notDetermined) {
            locationManager.requestWhenInUseAuthorization()
            
            // if you want the app to retrieve location data even in background, use requestAlwaysAuthorization
            //             locationManager.requestAlwaysAuthorization()
            return
        }
        
        if(status == .authorizedWhenInUse || status == .authorizedAlways) {
            // at this point the authorization status is authorized
            locationManager.startUpdatingLocation()
        }
    }
    
    /// Stop Location Updation
    func stopUpdating() {
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
    }
    
    
    /// Display Permission alert
    func displayAlertWithTitleMessageAndTwoButtons() {
        
        let alert = UIAlertController(title: "Settings",
                                      message: "Allow our app to access your location service to get accurate data",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Setting",
                                      style: .destructive,
                                      handler: { _ in
                                        let setting =  UIApplication.openSettingsURLString
                                        let bundle = Bundle.main.bundleIdentifier ?? ""
                                        let url = URL(string: "\(setting)\(bundle)")
                                        UIApplication.shared.open(url!, options: [:], completionHandler: nil) }))
        
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: .cancel,
                                      handler: nil))
        
        DispatchQueue.main.async {
            let root = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
            root?.present(alert, animated: true, completion: nil)
        }
    }
}

//MARK:- Location manager delegate methods
extension LocatorManager: CLLocationManagerDelegate {
    
    // called when the authorization status is changed for the core location permission
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if(status == .authorizedWhenInUse || status == .authorizedAlways){
            manager.startUpdatingLocation()
        }
                
        switch status {
        case .authorizedAlways:
            print("user allow app to get location data when app is active or in background")
        case .authorizedWhenInUse:
            print("user allow app to get location data only when app is active")
        case .denied:
            completion?(nil, "user tap 'disallow' on the permission dialog, cant get location data")
        case .restricted:
            completion?(nil, "parental control setting disallow location data")
        case .notDetermined:
            print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
        default:
            print("UnKnown")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        getLocationAddress(location, completion: completion)
        if mode == .oneShot { stopUpdating() }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        completion?(nil, error.localizedDescription)
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) { }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) { }
    
    
    func getLocationAddress(_ location: CLLocation, completion: CompletionHandler?) {
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            guard let _ = error else {
                guard let placemark = placemarks else { return }
                guard let place = placemark.first else { return }
                guard let coord = place.location?.coordinate else { return }
                completion?(UserLocation(coord: coord, cityName: place.locality ?? "", country: place.country ?? ""), nil)
                return
            }
        }
    }
    
    func getLocationByAddress(_ address:String, completion:CompletionHandler?) {
        CLGeocoder().geocodeAddressString(address) { (placemarks, error) in
            guard let _ = error else {
                guard let placemark = placemarks else { return }
                guard let place = placemark.first else { return }
                guard let coord = place.location?.coordinate else { return }
                completion?(UserLocation(coord: coord, cityName: place.locality ?? "", country: place.country ?? ""), nil)
                return
            }
        }
    }
}
