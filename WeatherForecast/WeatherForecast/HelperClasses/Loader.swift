//
//  Loader.swift
//  Loader
//
//  Created by Omar on 6/11/19.
//  Copyright © 2019 Roqay. All rights reserved.
//

import Foundation
import UIKit

struct Loader {
    static func show() {
        LoadingOverlay.shared.showOverlay()
    }
    static func hide() {
        LoadingOverlay.shared.hideOverlayView()
    }
}

private class LoadingOverlay {

    var overlayView : UIView!
    var gifImageView:UIImageView!
    var containerView:UIView!

    var activityIndicator : UIActivityIndicatorView!

    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }

    init() {
        self.containerView = UIView(frame: UIScreen.main.bounds)
        self.containerView.backgroundColor = .clear

        self.overlayView = UIView()
        overlayView.frame = CGRect(x: self.containerView.frame.width / 2 - 10,
                                   y: self.containerView.frame.height / 2 - 10,
                                   width: 20,
                                   height: 20)
        overlayView.clipsToBounds = true
        overlayView.layer.cornerRadius = 10
        overlayView.layer.zPosition = 1
        overlayView.backgroundColor = .clear

        self.activityIndicator = UIActivityIndicatorView()
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2)
        activityIndicator.style = .gray
        activityIndicator.color = .black
        overlayView.addSubview(activityIndicator)

        containerView.addSubview(overlayView)
    }

    public func showOverlay() {
        let view = UIApplication.shared.keyWindow
        overlayView.center = (view?.center)!
        view?.addSubview(containerView)
        activityIndicator.startAnimating()
    }

    public func hideOverlayView() {
        activityIndicator.stopAnimating()
        containerView.removeFromSuperview()
    }
}
