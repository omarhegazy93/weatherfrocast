//
//  UIView+StatusProvider.swift
//  HzmSanaSC
//
//  Created by Omar on 3/19/19.
//  Copyright © 2019 Omar. All rights reserved.
//

import Foundation
import UIKit
import StatusProvider

extension UIView: StatusController {
    
    public var statusView: StatusView? {
        let defaultView = DefaultStatusView()
        defaultView.titleLabel.font = .preferredFont(forTextStyle: .headline)
        defaultView.titleLabel.textColor = R.color.textBlue()!
        defaultView.descriptionLabel.font = .preferredFont(forTextStyle: .body)
        defaultView.descriptionLabel.textColor = R.color.greyDark()!
        return defaultView
    }
    
    func showHideState(count:Int?,
                       title: String? = "",
                       msg: String? = "No Result",
                       img: UIImage? = R.image.search()) {
        if count == nil || count == 0 {
            let newStatus = Status(title: title,
                                   description: msg,
                                   actionTitle: "",
                                   image: img)
            
            
            show(status: newStatus)
        }
        else {
            hideStatus()
        }
    }
}
