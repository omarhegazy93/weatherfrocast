//
//  AppRefrence.swift
//  OpenWeather
//
//  Created by Omar on 10/31/19.
//  Copyright © 2019 Omar. All rights reserved.
//

import UIKit
import SwiftMessages

struct AppRefrence {
    
    static func showSuccessMessage(title:String,
                                   body: String) {
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureTheme(.success)
        view.configureDropShadow()
        view.configureContent(title: title, body: body)
        view.button?.isHidden = true
        view.titleLabel?.isHidden = true
        view.titleLabel?.font = .preferredFont(forTextStyle: .title1)
        view.bodyLabel?.font = .preferredFont(forTextStyle: .body)
        //        view.button?.setTitle("hide", for: .normal)
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        //        view.buttonTapHandler = { _ in SwiftMessages.hide() }
        SwiftMessages.show(view: view)
    }
    
    static func showErrorMessage(title:String,
                                 body: String) {
        let view = MessageView.viewFromNib(layout: .messageView)
        view.configureTheme(.error)
        view.configureDropShadow()
        view.configureContent(title: title, body: body)
        view.titleLabel?.isHidden = true
        view.titleLabel?.font = .preferredFont(forTextStyle: .title1)
        view.bodyLabel?.font = .preferredFont(forTextStyle: .body)
        view.iconImageView?.isHidden = true
        //        view.button?.setTitle("hide", for: .normal)
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        view.button?.isHidden = true
        //        view.buttonTapHandler = { _ in SwiftMessages.hide() }
        SwiftMessages.show(view: view)
    }
    
    static var appWindow: UIWindow? {
        return UIApplication.shared.keyWindow
    }
    
    static var currentVC: UIViewController? {
        return (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController
    }
}
