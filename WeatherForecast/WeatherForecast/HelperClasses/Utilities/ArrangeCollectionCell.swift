//
//  ArrangeCollectionCell.swift
//  AlfHana
//
//  Created by Omar Hegazy on 6/14/19.
//  Copyright © 2019 Omar Hegazy. All rights reserved.
//

import Foundation
import UIKit

class FlowLayout: UICollectionViewFlowLayout {
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributesForElementsInRect = super.layoutAttributesForElements(in: rect)
        var newAttributesForElementsInRect = [UICollectionViewLayoutAttributes]()
        
        var rightMargin: CGFloat = rect.width;
        
        for attribute in attributesForElementsInRect! {
            attribute.frame.origin.x = rightMargin - attribute.frame.width - 8
            
            if attribute.frame.origin.x <= self.sectionInset.left {
                rightMargin = rect.width - self.sectionInset.right
                attribute.frame.origin.x = rightMargin - attribute.frame.width
            }
            rightMargin = rightMargin - attribute.frame.size.width - 8
            newAttributesForElementsInRect.append(attribute)
        }
        
        return newAttributesForElementsInRect
    }
}
