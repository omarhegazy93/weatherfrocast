//
//  RootWindowController.swift
//  AlfHana proj
//
//  Created by Omar Hegazy on 5/13/19.
//  Copyright © 2019 Omar Hegazy. All rights reserved.
//

import UIKit

struct RootWindowController {
    
    private static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    static func setRootWindowForHome() {
        let vc = R.storyboard.main.mainVC()!
        let nav = UINavigationController.init(rootViewController: vc)
        appDelegate.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
        appDelegate.window?.switchRootViewController(nav,
                                                     animated: true,
                                                     duration: 0.5,
                                                     options: UIView.AnimationOptions.transitionCrossDissolve,
                                                     completion: nil)
    }
    
    static func setRootWindowForIntro() {
                let vc = R.storyboard.main.introVC()!
                let nav = UINavigationController.init(rootViewController: vc)
        
                appDelegate.window?.backgroundColor = UIColor(red: 236.0, green: 238.0, blue: 241.0, alpha: 1.0)
                appDelegate.window?.switchRootViewController(nav,
                                                             animated: true,
                                                             duration: 0.5,
                                                             options: UIView.AnimationOptions.transitionFlipFromRight,
                                                             completion: nil)
    }
    
}




extension UIWindow {
    
    func switchRootViewController(_ viewController: UIViewController,
                                  animated: Bool = true,
                                  duration: TimeInterval = 0.5,
                                  options: UIView.AnimationOptions = .transitionFlipFromRight,
                                  completion: (() -> Void)? = nil) {
        guard animated else {
            rootViewController = viewController
            return
        }
        
        UIView.transition(with: self, duration: duration, options: options, animations: {
            let oldState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            self.rootViewController = viewController
            UIView.setAnimationsEnabled(oldState)
        }) { _ in
            completion?()
        }
    }
}
