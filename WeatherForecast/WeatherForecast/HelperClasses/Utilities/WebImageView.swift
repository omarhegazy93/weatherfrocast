//
//  WebImageView.swift
//  Nawaem
//
//  Created by Omar on 5/5/18.
//  Copyright © 2018 Omar. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func loadUserWebImage(imageUrl:String,placeHolderImage:String) {
        let urlStr = (imageUrl).addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        loadWebImage(imageUrl: urlStr!, placeHolder: UIImage(named: placeHolderImage)!)
    }

    func loadUserWebImage(imageUrl:String) {
        let urlStr = (imageUrl).addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        loadWebImage(imageUrl: urlStr!, placeHolder: R.image.placeholder()!)
    }
    
    func loadWebImageWithUrl(imageUrl:String) {
        let urlStr = imageUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        loadWebImage(imageUrl: urlStr!, placeHolder: R.image.placeholder()!)
    }
    
    func loadPlaceWebImageWithUrl(imageUrl:String) {
        let urlStr = imageUrl.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        loadWebImage(imageUrl: urlStr!, placeHolder: R.image.placeholder()!)
    }
    
    private func loadWebImage(imageUrl:String , placeHolder:UIImage) {
        self.startAnimating()
        self.kf.indicatorType = .activity
        let url = URL(string: imageUrl)
        self.kf.setImage(with: url,
                         placeholder:  placeHolder,
                         options: [.transition(.fade(1))])
    }
    
    func loadImage(imageUrl:String) {
        self.startAnimating()
        self.kf.indicatorType = .activity
        let url = URL(string: imageUrl)
        self.kf.setImage(with: url,
                         placeholder: R.image.placeholder()!,
                         options: [.transition(.fade(1))])
    }
}

extension UIButton{
    func loadUserImage(imageUrl:String) {
//        self.startAnimating()
//        self.kf.indicatorType = .activity
        guard let url = URL(string: imageUrl) else{ return }
        self.kf.setImage(with: url, for: .normal, placeholder: R.image.placeholder()!)
    }
}
