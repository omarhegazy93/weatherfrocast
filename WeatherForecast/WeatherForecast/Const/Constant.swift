//
//  Constant.swift
//  OpenWeather
//
//  Created by Omar Hegazy on 13/07/20.
//  Copyright © 2020 Omar Hegazy. All rights reserved.
//

import Foundation


struct APIConstants {
    static let BASE_URL = "http://api.openweathermap.org/data/2.5/"
    static let APP_ID = "3b32ffbd2b9353f270a209465427f681"
    static let IMAGE_URL = "http://openweathermap.org/img/wn/%@2x.png"
}

enum Units: String {
    case metric, imperial
}
