//
//  AppDelegate.swift
//  WeatherForecast
//
//  Created by Omar Hegazy on 7/13/20.
//  Copyright © 2020 Parent. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.setupNavigationBar()
        return true
    }

    func setupNavigationBar() {
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor =  .clear//UIColor(hexString: "EBEEFA")
        UIBarButtonItem.appearance().tintColor = .white
    }

}

extension UINavigationController {
   open override var preferredStatusBarStyle: UIStatusBarStyle {
      return topViewController?.preferredStatusBarStyle ?? .default
   }
}

