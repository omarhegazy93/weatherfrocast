//
//  SearchTableCell.swift
//  WeatherForecast
//
//  Created by Omar Hegazy on 7/14/20.
//  Copyright © 2020 Parent. All rights reserved.
//

import UIKit

class SearchTableCell: UITableViewCell {
    static let id = "SearchTableCell"
    
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var tempLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.accessoryType = .disclosureIndicator
    }

}
