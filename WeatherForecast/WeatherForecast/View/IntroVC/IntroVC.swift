//
//  IntroVC.swift
//  WeatherForecast
//
//  Created by Omar Hegazy on 7/13/20.
//  Copyright © 2020 Parent. All rights reserved.
//

import UIKit

class IntroVC: UIViewController {
    
    @IBOutlet weak var hightContraint: NSLayoutConstraint!
    @IBOutlet weak var widthContraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerStackView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var btn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLbl.alpha = 0
        self.btn.alpha = 0
        
        DispatchQueue.main.asyncAfter(deadline: .now()) {
            UIView.transition(with: self.titleLbl,
                              duration: 2,
                              options: .transitionCrossDissolve,
                              animations: {
                self.titleLbl.alpha = 1
            }) { (isComplete) in
                if isComplete{
                    UIView.transition(with: self.btn,
                                      duration: 1,
                                      options: .transitionFlipFromBottom,
                                      animations: {
                                        self.btn.alpha = 1
                    }, completion: nil)
                }
            }
        }
    }

    @IBAction func requestLocation(_ sender: UIButton){
        RootWindowController.setRootWindowForHome()
    }
    
    func getCurrentCity() {
        let vc = R.storyboard.main.searchVC()!
        self.navigationController?.pushViewController(vc, animated: true)
//        LocatorManager.shared.getCurrentLocationGeocoder { [weak self] (userLocation, error) in
//            guard let self = self else { return }
//            
////            if let _ = error {
////                self.currentCity.value = self.defaultLocation
////            }
////            else {
////                guard let location = userLocation else { return }
////                self.currentCity.value = MapperCity(gpsLocation: location)
////            }
////            self.saveCity(city: self.currentCity.value!)
//        }
    }

}
