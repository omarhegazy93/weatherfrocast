//
//  DayTableCell.swift
//  WeatherForecast
//
//  Created by Omar Hegazy on 7/14/20.
//  Copyright © 2020 Parent. All rights reserved.
//

import UIKit

class DayTableCell: UITableViewCell {
    static let id = "DayTableCell"
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    @IBOutlet weak var iconImg: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }


}
