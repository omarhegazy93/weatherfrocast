//
//  SelectedCityVC.swift
//  WeatherForecast
//
//  Created by Omar Hegazy on 7/14/20.
//  Copyright © 2020 Parent. All rights reserved.
//

import UIKit

class SelectedCityVC: UIViewController {
    
    @IBOutlet weak var weatherStatusLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var tempratureLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var daysTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.daysTableView.dataSource = self

    }
    
    @IBAction func dismissVC(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }

}

extension SelectedCityVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: DayTableCell.id) as! DayTableCell
        return cell
    }
}
