//
//  MainVC.swift
//  WeatherForecast
//
//  Created by Omar Hegazy on 7/14/20.
//  Copyright © 2020 Parent. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    @IBOutlet weak var citiesTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.citiesTableView.dataSource = self
//        self.citiesTableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Loader.show()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            Loader.hide()
        }
    }
    
    
}

extension MainVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CityTableCell.id) as! CityTableCell
        return cell
    }
}
