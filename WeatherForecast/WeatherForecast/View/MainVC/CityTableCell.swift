//
//  CityTableCell.swift
//  WeatherForecast
//
//  Created by Omar Hegazy on 7/14/20.
//  Copyright © 2020 Parent. All rights reserved.
//

import UIKit

class CityTableCell: UITableViewCell {
    static let id = "CityTableCell"
    
    @IBOutlet weak var currentTempLbl: UILabel!
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var cityNameLbl: UILabel!
    @IBOutlet weak var detailsLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.accessoryType = .disclosureIndicator
    }

}
